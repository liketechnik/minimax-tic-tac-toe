/*
 * SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>
 *
 * SPDX-License-Identifier: Unlicense
 */

% vim: ft=mercury

:- module tree.
:- interface.
:- import_module list, io, tictactoe.

:- type tree(T)
    --->    node(T, list(tree(T))).

:- pred is_leaf(tree(T)::in) is semidet.

:- pred display_tree(tree(tictactoe.state)::in, io::di, io::uo) is det.

:- implementation.
:- import_module string, int.

is_leaf(Tree) :-
    Tree = node(_, Children),
    is_empty(Children).

:- pred display_tree_indented(int::in, tree(tictactoe.state)::in, io::di, io::uo) is det.

display_tree_indented(Level, Tree, !IO) :-
    Tree = node(Current, Children),
    io.format("Level: %i\n", [i(Level)], !IO),
    display_state_field(Current, !IO),
    foldl((pred(C::in, !.IO::di, !:IO::uo) is det :-
        display_tree_indented(Level + 1, C, !IO),
        io.nl(!IO)), Children, !IO).

display_tree(Tree, !IO) :-
    display_tree_indented(0, Tree, !IO). 
