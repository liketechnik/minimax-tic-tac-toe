/*
 * SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>
 *
 * SPDX-License-Identifier: Unlicense
 */

% vim: ft=mercury

:- module main. 
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.

:- import_module tictactoe, tree, tree_builder, list, solutions, minimax, string, exception.

:- func into_position(character) = marker_positions is semidet.
into_position('1') = one.
into_position('2') = two.
into_position('3') = three.
into_position('4') = four.
into_position('5') = five.
into_position('6') = six.
into_position('7') = seven.
into_position('8') = eight.
into_position('9') = nine. 

:- pred player_turn(tictactoe.state::in, tictactoe.state::out, io::di, io::uo) is det.
player_turn(State, R, !IO) :-
    io.format("Player %c's turn. Position? ", [c(md(current(State)))], !IO),
    io.read_char(Input, !IO),
    ( if Input = ok(Char) then
        io.read_char(_, !IO),
        ( if Position = into_position(Char) then
            ( if Turn = place_marker(State, Position) then
                R = Turn
            else
                io.print_line("Invalid position!", !IO),
                player_turn(State, R, !IO)
            )
        else
            io.print_line("Invalid input. Allowed: 1-9", !IO),
            player_turn(State, R, !IO)
        )
    else
        throw("Failed to read from stdin")
    ).

:- pred player_loop(tictactoe.state::in, io::di, io::uo) is det.
:- pred computer_loop(tictactoe.state::in, io::di, io::uo) is det.
:- pred game_loop(tictactoe.state, pred(tictactoe.state, io, io), io, io).
:- mode game_loop(in, pred(in, di, uo) is det, di, uo).

player_loop(State, !IO) :-
    player_turn(State, NewState, !IO),
    game_loop(NewState, computer_loop, !IO).

computer_loop(State, !IO) :-
    game_loop(minimax(State), player_loop, !IO).

game_loop(State, Loop, !IO) :- 
    display_state_field(State, !IO),
    ( if terminal_test(State, x) then
        io.format("Player %c won\n", [c(md(tictactoe.x))], !IO)
    else if terminal_test(State, o) then
        io.format("Player %c won\n", [c(md(tictactoe.o))], !IO)
    else if terminal_test(State) then
        io.print_line("Draw", !IO)
    else
        io.print_line("", !IO),
        call(Loop, State, !IO) 
    ).

main(!IO) :- 
    io.print_line("Tictactoe", !IO),
    State = game_start(x),
    game_loop(State, player_loop, !IO).
