<!--
SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>

SPDX-License-Identifier: CC0-1.0
-->

# Minimax Tic Tac Toe

Implementation of Minimax algorithm in mercury (https://mercurylang.org), optionally using alpha-beta-pruning.

