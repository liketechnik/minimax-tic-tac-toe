/*
 * SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>
 *
 * SPDX-License-Identifier: Unlicense
 */

% vim: ft=mercury

:- module tictactoe.
:- interface.
:- import_module io.

:- type marker_positions
    --->    one
    ;       two
    ;       three
    ;       four
    ;       five
    ;       six
    ;       seven
    ;       eight
    ;       nine.


:- type markers
    --->    x
    ;       o
    ;       empty.

:- type field
    --->    field(
                '1' :: markers,
                '2' :: markers,
                '3' :: markers,
                '4' :: markers,
                '5' :: markers,
                '6' :: markers,
                '7' :: markers,
                '8' :: markers,
                '9' :: markers
            ).

:- type state.

:- func empty_field = field.

:- func game_start(markers) = tictactoe.state.
:- func current(tictactoe.state) = markers.
:- func place_marker(tictactoe.state, marker_positions) = tictactoe.state is semidet.

:- func md(markers) = character. 
:- pred display_field(field::in, io::di, io::uo) is det.
:- pred display_state_field(tictactoe.state::in, io::di, io::uo) is det.

:- pred terminal_test(tictactoe.state::in) is semidet.
% check win condition for a player
:- pred terminal_test(tictactoe.state::in, markers::in) is semidet.
:- func utility(tictactoe.state, markers) = int is det.
:- func utility_markers(tictactoe.state) = int.

:- implementation.
:- import_module list, string, char, int.

:- type state
    --->    state(
                field :: field,
                current_player :: markers
            ).

md(x) = 'X'.
md(o) = 'O'.
md(empty) = ' '.

:- func other(markers) = markers is semidet.
other(x) = o.
other(o) = x.

empty_field = field(
    empty, empty, empty,
    empty, empty, empty,
    empty, empty, empty 
).

game_start(Marker) = state(empty_field, Marker).
current(State) = State^current_player.

display_field(Field, !IO) :-
    Separator = "---+---+---",
    io.format(" %c | %c | %c \n%s\n %c | %c | %c \n%s\n %c | %c | %c \n",
        [c(md(Field^'1')), c(md(Field^'2')), c(md(Field^'3')), s(Separator), c(md(Field^'4')), c(md(Field^'5')), c(md(Field^'6')), s(Separator), c(md(Field^'7')), c(md(Field^'8')), c(md(Field^'9'))],
        !IO).

display_state_field(State, !IO) :-
    display_field(State^field, !IO).

place_marker(State, one) = R :- 
    State^field^'1' = empty,
    MarkerPlaced = State^field^'1' := State^current_player,
    R = MarkerPlaced^current_player := other(MarkerPlaced^current_player).

place_marker(State, two) = R :- 
    State^field^'2' = empty,
    MarkerPlaced = State^field^'2' := State^current_player,
    R = MarkerPlaced^current_player := other(MarkerPlaced^current_player).

place_marker(State, three) = R :- 
    State^field^'3' = empty,
    MarkerPlaced = State^field^'3' := State^current_player,
    R = MarkerPlaced^current_player := other(MarkerPlaced^current_player).

place_marker(State, four) = R :- 
    State^field^'4' = empty,
    MarkerPlaced = State^field^'4' := State^current_player,
    R = MarkerPlaced^current_player := other(MarkerPlaced^current_player).

place_marker(State, five) = R :- 
    State^field^'5' = empty,
    MarkerPlaced = State^field^'5' := State^current_player,
    R = MarkerPlaced^current_player := other(MarkerPlaced^current_player).

place_marker(State, six) = R :- 
    State^field^'6' = empty,
    MarkerPlaced = State^field^'6' := State^current_player,
    R = MarkerPlaced^current_player := other(MarkerPlaced^current_player).

place_marker(State, seven) = R :- 
    State^field^'7' = empty,
    MarkerPlaced = State^field^'7' := State^current_player,
    R = MarkerPlaced^current_player := other(MarkerPlaced^current_player).

place_marker(State, eight) = R :- 
    State^field^'8' = empty,
    MarkerPlaced = State^field^'8' := State^current_player,
    R = MarkerPlaced^current_player := other(MarkerPlaced^current_player).

place_marker(State, nine) = R :- 
    State^field^'9' = empty,
    MarkerPlaced = State^field^'9' := State^current_player,
    R = MarkerPlaced^current_player := other(MarkerPlaced^current_player).

terminal_test(State, Marker) :- 
    Field = State^field,
    (
        Field^'1' = Marker,
        Field^'2' = Marker,
        Field^'3' = Marker
    ;
        Field^'4' = Marker,
        Field^'5' = Marker,
        Field^'6' = Marker
    ;
        Field^'7' = Marker,
        Field^'8' = Marker,
        Field^'9' = Marker 
    ;
        Field^'1' = Marker,
        Field^'4' = Marker,
        Field^'7' = Marker 
    ;
        Field^'2' = Marker,
        Field^'5' = Marker,
        Field^'8' = Marker
    ;
        Field^'3' = Marker,
        Field^'6' = Marker,
        Field^'9' = Marker
    ;
        Field^'1' = Marker,
        Field^'5' = Marker,
        Field^'9' = Marker
    ;
        Field^'3' = Marker,
        Field^'5' = Marker,
        Field^'7' = Marker
    ).

terminal_test(State) :-
    Field = State^field,
    (
        terminal_test(State, x) 
    ;
        terminal_test(State, o)
    ;
        % can the game continue?
        not (Field^'1' = empty),
        not (Field^'2' = empty),
        not (Field^'3' = empty),
        not (Field^'4' = empty),
        not (Field^'5' = empty),
        not (Field^'6' = empty),
        not (Field^'7' = empty),
        not (Field^'8' = empty),
        not (Field^'9' = empty)
    ).

utility_markers(State) = R :-
    Field = State^field,
    Unused = [],
    Field1 = (if Field^'1' = empty then 
        Unused ++ [1]
        else Unused),
    Field2 = (if Field^'2' = empty then 
        Field1 ++ [1]
        else Field1),
    Field3 = (if Field^'3' = empty then 
        Field2 ++ [1]
        else Field2),
    Field4 = (if Field^'4' = empty then
        Field3 ++ [1]
        else Field3),
    Field5 = (if Field^'5' = empty then 
        Field4 ++ [1]
        else Field4),
    Field6 = (if Field^'6' = empty then
        Field5 ++ [1]
        else Field5),
    Field7 = (if Field^'7' = empty then 
        Field6 ++ [1]
        else Field6),
    Field8 = (if Field^'8' = empty then
        Field7 ++ [1]
        else Field7),
    Field9 = (if Field^'9' = empty then
        Field8 ++ [1]
        else Field8),
    R = foldl((func(E, A) = A + E), Field9, 1).
    % trace [io(!IO)] (
    %     io.format("[TRACE] Utility: %i\n", [i(R)], !IO),
    %     display_field(Field, !IO)
    % ).


utility(State, CurrentPlayer) = R :-
    ( if terminal_test(State, CurrentPlayer) then
        R = utility_markers(State)
    else if terminal_test(State, other(CurrentPlayer)) then
        R = -1 * utility_markers(State)
    else
        R = 0
    ).
