/*
 * SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>
 *
 * SPDX-License-Identifier: Unlicense
 */

:- module computer.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.

:- import_module tictactoe, string, minimax, list, char.

:- pred game_loop(tictactoe.state::in, io::di, io::uo) is det.

game_loop(State, !IO) :-
    display_state_field(State, !IO),
    ( if terminal_test(State, x) then
        io.format("Player %c won\n", [c(md(tictactoe.x))], !IO)
    else if terminal_test(State, o) then
        io.format("Player %c won\n", [c(md(tictactoe.o))], !IO)
    else if terminal_test(State) then
        io.print_line("Draw", !IO)
    else
        io.print_line("", !IO),
        game_loop(minimax(State), !IO)
    ).

main(!IO) :-
    State = game_start(x),
    game_loop(State, !IO).
