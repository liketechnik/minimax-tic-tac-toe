/*
 * SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>
 *
 * SPDX-License-Identifier: Unlicense
 */

:- module minimax.
:- interface.
:- import_module tictactoe.

:- func minimax(tictactoe.state) = state.

:- func max_value(tictactoe.state, markers, int, int) = int.
:- func min_value(tictactoe.state, markers, int, int) = int.

:- implementation.
:- import_module list, solutions, int, string, tree_builder.

:- mutable(branch_counter, int, 0, ground, [untrailed]).
:- mutable(branch_counter_total, int, 0, ground, [untrailed]).

:- pred pruning is semidet.
pruning :- true. 

:- pred max_acc(state::in, int::in, int::out, int::in, int::out, int::in, int::out, markers::in, markers::out) is det.
max_acc(S, Val, RVal, Alpha, RAlpha, Beta, RBeta, CurrentPlayer, RCurrentPlayer) :-
    % those two never change
    RBeta = Beta,
    RCurrentPlayer = CurrentPlayer,

    % pruning: fake early return by simply not changing any values (and not evaluating current state)
    ( if pruning, Val >= Beta then
        RVal = Val,
        RAlpha = Alpha
   % otherwise just do the usual algoritm
    else
        V = int.max(min_value(S, CurrentPlayer, Alpha, Beta), Val),
        RAlpha = int.max(Alpha, V),
        RVal = V
    ).

:- pred min_acc(state::in, int::in, int::out, int::in, int::out, int::in, int::out, markers::in, markers::out) is det.
min_acc(S, Val, RVal, Alpha, RAlpha, Beta, RBeta, CurrentPlayer, RCurrentPlayer) :-
    % those two never change
    RAlpha = Alpha,
    RCurrentPlayer = CurrentPlayer,

    % pruning: fake early return by simply not changing any values (and not evaluating current state)
    ( if pruning, Val =< Alpha then
        RVal = Val,
        RBeta = Beta
   % otherwise just do the usual algoritm
    else
        V = int.min(max_value(S, CurrentPlayer, Alpha, Beta), Val),
        RBeta = int.min(Beta, V),
        RVal = V
    ).

max_value(State, CurrentPlayer, Alpha, Beta) = R :- 
    trace [ state(branch_counter, !BranchCounter) ] (!:BranchCounter = !.BranchCounter + 1),
    ( if terminal_test(State) then
        R = utility(State, CurrentPlayer)
    else
        solutions((pred(Next::out) is nondet :-
            turn(State, Next)), Turns),
        foldl4(max_acc, Turns, int.min_int, R, Alpha, _, Beta, _, CurrentPlayer, _)
    ).
    % trace [io(!IO)] (
    %     io.format("[TRACE] max_value: %i\n", [i(R)], !IO)
    % ).

min_value(State, CurrentPlayer, Alpha, Beta) = R :- 
    trace [ state(branch_counter, !BranchCounter) ] (!:BranchCounter = !.BranchCounter + 1),
    ( if terminal_test(State) then
        R = utility(State, CurrentPlayer)
    else
        solutions((pred(Next::out) is nondet :-
            turn(State, Next)), Turns),
        foldl4(min_acc, Turns, int.max_int, R, Alpha, _, Beta, _, CurrentPlayer, _)
    ).
    % trace [io(!IO)] (
    %     io.format("[TRACE] min_value: %i\n", [i(R)], !IO)
    % ).

:- pred minimax_acc(markers::in, state::in, int::in, int::out, state::in, state::out, int::in, int::out, int::in, int::out) is det.
minimax_acc(CurrentPlayer, S, Val, RVal, Action, RAction, Alpha, RAlpha, Beta, RBeta) :-
    RBeta = Beta, % beta never changes

    % pruning: fake early return by simply not changing any values (and not evaluating the current state)
    ( if pruning, Val >= Beta then
        RVal = Val,
        RAction = Action,
        RAlpha = Alpha
   % otherwise just do the usual algoritm
    else
        V = min_value(S, CurrentPlayer, Alpha, Beta),
        ( if Val =< V then
            RVal = V,
            RAction = S
        else
            RVal = Val,
            RAction = Action
        ),
        RAlpha = int.max(Alpha, V)
    ).

minimax(State) = R :-
    trace [ io(!IO), state(branch_counter, !BranchCounter) ] (
        !:BranchCounter = 0
    ),
    solutions((pred(Next::out) is nondet :-
        turn(State, Next)), Turns),
    foldl4(minimax_acc(current(State)), Turns, int.min_int, _, State, R, int.min_int, _, int.max_int, _),
    trace [ io(!IO), state(branch_counter, !BranchCounter), state(branch_counter_total, !BranchCounterTotal) ] (
        !:BranchCounterTotal = !.BranchCounterTotal + !.BranchCounter,
        io.format("[TRACE] Branches taken: %i\n[TRACE] Total branches taken: %i\n", [i(!.BranchCounter), i(!.BranchCounterTotal)], !IO)
        % io.format("Utility of selected: %i\n", [i(U)], !IO)
    ).
