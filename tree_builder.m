/*
 * SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>
 *
 * SPDX-License-Identifier: Unlicense
 */

:- module tree_builder.
:- interface.

:- import_module tictactoe, tree.

:- pred marker_position(marker_positions::out) is multi.

:- pred turn(state::in, state::out) is nondet.

:- pred turns(state::in, tree(state)::out) is det.

:- implementation.
:- import_module list, solutions.

marker_position(one).
marker_position(two).
marker_position(three).
marker_position(four).
marker_position(five).
marker_position(six).
marker_position(seven).
marker_position(eight).
marker_position(nine).

turn(Current, Next) :-
    marker_position(Position),
    Next = place_marker(Current, Position).

turns(Current, Tree) :- 
    % get all possible turns
    solutions((pred(Next::out) is nondet :-
        turn(Current, Next)), Turns),
    % recursively get all possible turns for the turns just found
    Children = map((func(S) = T :- 
        turns(S, T)), Turns),
    % hooray, tree is finished
    Tree = node(Current, Children).
